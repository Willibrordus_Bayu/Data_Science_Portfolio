# Data_Science_Portfolio
Summary of data science case

# [Project I: Data Science Employee Future in Company](https://github.com/Bayunova28/Data_Science_Portfolio/blob/main/employee-future-prediction.ipynb)
* Data source : https://www.kaggle.com/tejashvi14/employee-future-prediction
* Predict employee in the future on company
* Build multi-class machine learning model (Linear Discriminant Analysis, K-Nearest Neighbors, Decision Tree, Random Forest, Naive Bayes, Support Vector Machine)
* Evaluate Random Forest Model on accuracy score 82%
* Visualize insights

![](https://github.com/Bayunova28/Data_Science_Portfolio/blob/main/images/__results___8_0.png)
![](https://github.com/Bayunova28/Data_Science_Portfolio/blob/main/images/random%20forest.png)
![](https://github.com/Bayunova28/Data_Science_Portfolio/blob/main/images/__results___29_0.png)



# [Project II: Data Science Tour & Travels Customer](https://github.com/Bayunova28/Data_Science_Portfolio/blob/main/tour-travels-customer-churn.ipynb)
* Data source : https://www.kaggle.com/tejashvi14/tour-travels-customer-churn-prediction
* Predict customer churn who will tour & travel
* Build multi-class machine learning model (K-Nearest Neighbors, Support Vector Machine, Linear Support Vector Machine, Decision Tree, Random Forest, Extreme Gradient Boosting, Adaptive Boosting, Gradient Boosting, Naive Bayes, Linear Discriminant Analysis, Quadratic Discriminant Analysis)
* Evaluate Extreme Gradient Boosting Model on accuracy score 88%
* Visualize insights

![](https://github.com/Bayunova28/Data_Science_Portfolio/blob/main/images/u.png)
![](https://github.com/Bayunova28/Data_Science_Portfolio/blob/main/images/__results___15_0.png)
![](https://github.com/Bayunova28/Data_Science_Portfolio/blob/main/images/__results___16_0.png)
![](https://github.com/Bayunova28/Data_Science_Portfolio/blob/main/images/__results___21_1.png)



# [Project III: Data Science Diabetes Health Indicators](https://github.com/Bayunova28/Data_Science_Portfolio/blob/main/diabetes-health-indicators.ipynb)
* Data source : https://www.kaggle.com/alexteboul/diabetes-health-indicators-dataset
* Predict Diabetes Indicators on patient in hospital
* Build Random Forest model 
* Evaluate Random Forest Model on accuracy score 85%
* Visualize insights

![](https://github.com/Bayunova28/Data_Science_Portfolio/blob/main/images/__results___9_0.png)
![](https://github.com/Bayunova28/Data_Science_Portfolio/blob/main/images/__results___21_1.png)
![](https://github.com/Bayunova28/Data_Science_Portfolio/blob/main/images/__results___19_0.png)



# [Project IV: Data Science Melbourne House Price](https://github.com/Bayunova28/Data_Science_Portfolio/blob/main/melbourne-housing.ipynb)
* Data source : https://www.kaggle.com/peterkmutua/housing-dataset
* Predict Housing Price at Melbourne
* Build Linear Regression model 
* Evaluate Linear Regression Model on accuracy score 79%
* Visualize insights

![](https://github.com/Bayunova28/Data_Science_Portfolio/blob/main/images/o.png)
![](https://github.com/Bayunova28/Data_Science_Portfolio/blob/main/images/j.png)
![](https://github.com/Bayunova28/Data_Science_Portfolio/blob/main/images/k.png)
![](https://github.com/Bayunova28/Data_Science_Portfolio/blob/main/images/r.png)
![](https://github.com/Bayunova28/Data_Science_Portfolio/blob/main/images/g.png)
![](https://github.com/Bayunova28/Data_Science_Portfolio/blob/main/images/__results___26_1.png)
![](https://github.com/Bayunova28/Data_Science_Portfolio/blob/main/images/__results___27_1.png)



# [Project V: Data Science HR Changing Jobs of Employee](https://github.com/Bayunova28/Data_Science_Portfolio/blob/main/hr-changing-jobs-of-employee.ipynb)
* Data source : https://www.kaggle.com/kukuroo3/hr-data-predict-change-jobscompetition-form
* Predict Human Resource to change job on employee
* Build multi-class machine learning model (K-Nearest Neighbors, Linear Support Vector Machine, Decision Tree, Random Forest, Extreme Gradient Boosting, Adaptive Boosting, Naive Bayes, Linear Discriminant Analysis, Quadratic Discriminant Analysis) 
* Evaluate Adaptive Boosting Model on accuracy score 86%
* Visualize insights

![](https://github.com/Bayunova28/Data_Science_Portfolio/blob/main/images/__results___14_0.png)
![](https://github.com/Bayunova28/Data_Science_Portfolio/blob/main/images/__results___39_0.png)
![](https://github.com/Bayunova28/Data_Science_Portfolio/blob/main/images/__results___39_1.png)
![](https://github.com/Bayunova28/Data_Science_Portfolio/blob/main/images/2.png)
![](https://github.com/Bayunova28/Data_Science_Portfolio/blob/main/images/4.png)
![](https://github.com/Bayunova28/Data_Science_Portfolio/blob/main/images/__results___23_0.png)
![](https://github.com/Bayunova28/Data_Science_Portfolio/blob/main/images/__results___25_0.png)
![](https://github.com/Bayunova28/Data_Science_Portfolio/blob/main/images/__results___26_0.png)
![](https://github.com/Bayunova28/Data_Science_Portfolio/blob/main/images/__results___27_0.png)
![](https://github.com/Bayunova28/Data_Science_Portfolio/blob/main/images/s.png)


# [Project VI: Data Science Used Car Price](https://github.com/Bayunova28/Data_Science_Portfolio/blob/main/used-car-price.ipynb)
* Data source : https://www.kaggle.com/kukuroo3/used-car-price-dataset-competition-format
* Predict Car Price
* Build Linear Regression Model
* Evaluate Linear Regression Model on accuracy score 88%
* Visualize insights

![](https://github.com/Bayunova28/Data_Science_Portfolio/blob/main/images/__results___12_0.png)
![](https://github.com/Bayunova28/Data_Science_Portfolio/blob/main/images/op.png)
![](https://github.com/Bayunova28/Data_Science_Portfolio/blob/main/images/gh.png)
![](https://github.com/Bayunova28/Data_Science_Portfolio/blob/main/images/__results___21_0.png)
![](https://github.com/Bayunova28/Data_Science_Portfolio/blob/main/images/y.png)
![](https://github.com/Bayunova28/Data_Science_Portfolio/blob/main/images/__results___29_1.png)
![](https://github.com/Bayunova28/Data_Science_Portfolio/blob/main/images/f.png)



# [Project VII: Data Science Arcane Soundtrack Lyrics](https://github.com/Bayunova28/Data_Science_Portfolio/blob/main/arcane-soundtrack-lyrics.ipynb)
* Data source : https://www.kaggle.com/brandonqilin/arcane-soundtrack-lyrics
* Predict Act (the current narrative of the story)
* Build sentiment analysis for natural language processing on text data
* Build K-Nearest Neighbors Model
* Evaluate K-Nearest Neighbors Model on accuracy score 86%
* Visualize insights

![](https://github.com/Bayunova28/Data_Science_Portfolio/blob/main/images/te.png)
![](https://github.com/Bayunova28/Data_Science_Portfolio/blob/main/images/ok.png)
![](https://github.com/Bayunova28/Data_Science_Portfolio/blob/main/images/tq.png)
![](https://github.com/Bayunova28/Data_Science_Portfolio/blob/main/images/ol.png)
![](https://github.com/Bayunova28/Data_Science_Portfolio/blob/main/images/tg.png)
![](https://github.com/Bayunova28/Data_Science_Portfolio/blob/main/images/th.png)
![](https://github.com/Bayunova28/Data_Science_Portfolio/blob/main/images/tr.png)
![](https://github.com/Bayunova28/Data_Science_Portfolio/blob/main/images/ty.png)
![](https://github.com/Bayunova28/Data_Science_Portfolio/blob/main/images/we.png)
![](https://github.com/Bayunova28/Data_Science_Portfolio/blob/main/images/lm.png)
